class ApplicationMailer < ActionMailer::Base
  default from: "feedback@bossmesser.com"
  layout 'mailer'
end
