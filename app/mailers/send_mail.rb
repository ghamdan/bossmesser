class SendMail < ApplicationMailer

    def sendcode(email, mailcode)
        email = email
        @code = mailcode.code
        @domains = mailcode.domain
        @url = code_checker_url(@code)

        mail(to: email, subject: 'BossMesser - Here is your confirmation code.')
    end

    def send_cancel_token(email, token)
        @url = cancel_boss_message_url(:token => token)
        mail(to: email, subject: 'BossMesser - Your cancel link in case you change your mind.')
    end

    def sendbossmessage(message)
        email = message.to
        @message = Obscenity.sanitize(message.message)

        mail(to: email, subject: 'BossMesser - You Have a anonym feedback from a employee')

    end

    def sendbosscopymessage(bcc, message)
        @message = Obscenity.sanitize(message.message)

        mail(to: bcc, subject: 'BossMesser - You message has been sent to your boss anonymously')

    end
end
