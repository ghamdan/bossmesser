class InicialController < ApplicationController
  rescue_from Paypal::Exception::APIError, with: :paypal_api_error

  def index

  end

  def check_you_mail
      @email = params[:youremail]
      @domain = params[:youremail].partition('@').last
      @mailcode = MailCode.create(domain: @domain, fromail: @email)

      SendMail.sendcode(@email,@mailcode).deliver_now

  end

  def codecheck
      @domain = MailCode.find_by_code(params[:code])

      if @domain
          if @domain.used
              redirect_to codeused_path
          else
              @message = @domain.build_message
          end
      else
          redirect_to codeused_path
      end

  end

  def codeused

  end

  def thankyou

  end

  def pay_for_message
      @domain = MailCode.find_by_code(params[:code])

      if @domain.used
          redirect_to codeused_path
      else
          @message = @domain.build_message(to: "#{params[:message][:to]}@#{@domain.domain}", message: params[:message][:message]).save

          if Rails.env.production?
              require 'geoip'
              currency = GeoIP.new("#{Rails.root}/db/GeoIP.dat").country(request.remote_ip).country_name == "Brazil" ? :BRL : :USD

              paypal_options = {
                  no_shipping: true, # if you want to disable shipping information
                  allow_note: false, # if you want to disable notes
                  pay_on_paypal: true # if you don't plan on showing your own confirmation step
              }

              request = Paypal::Express::Request.new(
                  :username   => 'contact_api2.bossmesser.com',
                  :password   => 'SR2HER6C7Z53VJ2L',
                  :signature  => 'ALqNpbZpva18Rgg6Pv5ifOTOyWHdAnSs1SOhpi5P0r7EXgv6-CxvYbxM'
              )
              payment_request = Paypal::Payment::Request.new(
                  :currency_code => currency,   # if nil, PayPal use USD as default
                  :description   => "Send a Anonym Feedback to your Boss",    # item description
                  :quantity      => 1,      # item quantity
                  :amount        => 1.00,   # item value
                  :custom_fields => {
                      CARTBORDERCOLOR: "FFFF66",
                      LOGOIMG: "http://www.bossmesser.com/bossmesser-mail.png"
                  }
              )

              response = request.setup(
                  payment_request,
                  deliver_boss_message_url,
                  error_url,
                  paypal_options  # Optional
              )

              @domain.update_attribute(:token, response.token)

              redirect_to response.redirect_uri

          else
              redirect_to deliver_boss_message_path(:token => @domain.code)
          end


      end
  end

  def error

  end

  def deliver_boss_message

      if Rails.env.production?
          require 'geoip'
          currency = GeoIP.new("#{Rails.root}/db/GeoIP.dat").country(request.remote_ip).country_name == "Brazil" ? :BRL : :USD

          request = Paypal::Express::Request.new(
              :username   => 'contact_api2.bossmesser.com',
              :password   => 'SR2HER6C7Z53VJ2L',
              :signature  => 'ALqNpbZpva18Rgg6Pv5ifOTOyWHdAnSs1SOhpi5P0r7EXgv6-CxvYbxM'
          )

          token = params[:token]
          payer_id = params[:PayerID]
          payment_request = Paypal::Payment::Request.new(
              :currency_code => currency,   # if nil, PayPal use USD as default
              :description   => "Send a Anonym Feedback to your Boss",    # item description
              :quantity      => 1,      # item quantity
              :amount        => 1.00,   # item value
              :custom_fields => {
                  CARTBORDERCOLOR: "FFFF66",
                  LOGOIMG: "http://www.bossmesser.com/bossmesser-mail.png"
              }
          )

          @domain = MailCode.find_by_token(token)

          response = request.checkout!(
              token,
              payer_id,
              payment_request
          )
      else
          @domain = MailCode.find_by_code(params[:token])
      end

      SendMail.send_cancel_token(@domain.fromail, @domain.code).deliver_now

      SendMail.delay(run_at: 2.days.from_now, token: @domain.code).sendbossmessage(@domain.message)

      SendMail.delay(run_at: 2.days.from_now).sendbosscopymessage(@domain.fromail, @domain.message)

      @domain.update_attribute(:used, true)

      redirect_to thankyou_path
  end

  def cancel_mail_send
      token = params[:token]

      @email = Delayed::Job.find_by_token(token)

      if @email
          @email.destroy
          @message = "The email to your boss will not be delivered. Glad to see that you are in peace!"
      else
          @message = "This token is invalid and this message is not on our queue. If there are more than 2 days, it's already delivered. If not, good right?"

      end

  end

  def about

  end

  def terms

  end

  def paypal_api_error(e)
      errors = e.response.details.collect(&:long_message).join('<br />')
      p errors

      redirect_to error_path, flash[:errors] = errors
  end
end
