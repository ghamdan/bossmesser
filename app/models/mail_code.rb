class MailCode < ActiveRecord::Base
    before_create :generate_token

    has_one :message


    def generate_token
        self.code = loop do
            random_token = SecureRandom.urlsafe_base64(nil, false)
            break random_token unless MailCode.exists?(code: random_token)
        end

        self.sec_code = loop do
            random_token = SecureRandom.urlsafe_base64(nil, false)
            break random_token unless MailCode.exists?(sec_code: random_token)
        end
    end
end
