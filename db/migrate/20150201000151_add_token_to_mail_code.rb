class AddTokenToMailCode < ActiveRecord::Migration
  def change
    add_column :mail_codes, :token, :string
  end
end
