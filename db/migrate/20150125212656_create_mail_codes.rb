class CreateMailCodes < ActiveRecord::Migration
  def change
    create_table :mail_codes do |t|
      t.string :domain
      t.string :code

      t.timestamps null: false
    end
  end
end
