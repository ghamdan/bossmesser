class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :to
      t.text :message
      t.belongs_to :mail_code, index: true

      t.timestamps null: false
    end
    add_foreign_key :messages, :mail_codes
  end
end
