class AddUsedToMailCode < ActiveRecord::Migration
  def change
    add_column :mail_codes, :used, :boolean, :default => false
  end
end
