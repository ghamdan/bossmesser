class AddSecondCodeToMailCode < ActiveRecord::Migration
  def change
    add_column :mail_codes, :sec_code, :string
  end
end
