Rails.application.routes.draw do

  get '/codecheck/:code' => 'inicial#codecheck', :as => 'code_checker'

  get 'expiredcode' => 'inicial#codeused', :as => 'codeused'

  post 'check_your_mail' => 'inicial#check_you_mail', :as => 'check_your_mail'

  get 'thankyou' => 'inicial#thankyou', :as => 'thankyou'

  post 'payformessage' => 'inicial#pay_for_message', :as => 'pay_for_message'

  get 'delivered/' => 'inicial#deliver_boss_message', :as => 'deliver_boss_message'

  get 'cancel/:token' => 'inicial#cancel_mail_send', :as => 'cancel_boss_message'

  get 'error' => 'inicial#error', :as => 'error'

  get 'about' => 'inicial#about', :as => 'about'
  get 'terms_and_conditions' => 'inicial#terms', :as => 'terms'

  root 'inicial#index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
